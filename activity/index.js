console.log("hello world");

let num = 4
const getCube = num**3;
console.log(`The cube of ${num} is ${getCube}`);


let address = ["1505", "Alvarez St.", "Sta. Cruz", "Manila"];
const [ houseNumber, streetName, municipalityName, cityName ] = address;
console.log(houseNumber);
console.log(streetName);
console.log(municipalityName);
console.log(cityName);
console.log(`${houseNumber} ${streetName} ${municipalityName}, ${cityName}`);


let animal = {
	mammals: "dog",
	birds: "penguin",
	reptiles:"anaconda",
	invertebrates: "cicada"
}
const {mammals, birds, reptiles, invertebrates} = animal;
console.log(mammals);
console.log(birds);
console.log(reptiles);
console.log(invertebrates);
console.log(`Some examples of animals are ${mammals}, ${birds}, ${reptiles} and ${invertebrates}.`);


const arrayNums = [54, 9, 4]
arrayNums.forEach(x => console.log(x));


const reduceNumber = [22, 10, 3].reduce((partSum, a) => partSum + a, 0);
console.log(reduceNumber); 


class Dog{
	constructor(name, age, breed){
		this.name = name,
		this.age = age, 
		this.breed = breed
	}
};

const dog1 = new Dog("Alpine", 1, "Beagle");
console.log(dog1);
const dog2 = new Dog("Bimby", 2, "Golden Retriever");
console.log(dog2);


//==========================================
// console.log(
//   [1, 2, 3, 4].reduce((a, b) => a + b, 0)
// )
// console.log(
//   [].reduce((a, b) => a + b, 0)
// )